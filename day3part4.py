import requests
import datetime
import reverse_geocoder

URL= "http://api.open-notify.org/iss-now.json"

def main():
    resp = requests.get(URL).json()
    
    longi = resp["iss_position"]['longitude']
    lati = resp["iss_position"]['latitude']
    
    ts = resp['timestamp']
    ts = datetime.datetime.fromtimestamp(ts)

    location = reverse_geocoder.search((longi, lati))

    city = location[0]["name"]
    country = location[0]["cc"]


    print(f"""CURRENT LOCATION OF THE ISS:
    Lon: {long}
    Lat: {lati}
    City/Country: {city}, {country}
    """)

if __name__ == "__main__":
    main()
