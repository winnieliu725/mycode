from flask import Flask
from flask import session
from flask import render_template
from flask import redirect
from flask import url_for
from flask import escape
from flask import request

app = Flask(__name__)

x = "Winnie"

@app.route("/")
def welcome():
    # some code gets executed
    # something gets returned back to the clients
    return render_template("basic.html")
