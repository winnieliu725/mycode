
def instructions():
    print('''
    Harry Potter: Hogwarts Adventure
    ================================
    Commands:
    go [floor x]
    get [item]
    ''')

def showStatus():
    print('---------')
    print('You are in the ' + currentfloor)
    print('Bag: '+ str(bag))
    if "item" in floors[currentfloor]:
        print('You see the ' + floors[currentfloor]['item'])
    print('---------')

bag = []

floors ={

        "The Potion Classroom": { 
            "floor 1": "Hall",
            "floor 2": "Girl's Bathroom",
            "floor 3": "Gryffindor Commonroom",
            "floor 4": "Transfiguration Classroom",
            "floor 5": "Headmaster's Office"},
        

        "Hall":{
            "basement": "The Potion Classroom", 
            "floor 2": "Girl's Bathroom",
            "floor 3": "Gryffindor Commonroom",
            "floor 4": "Transfiguration Classroom",
            "floor 5": "Headmaster's Office", 
            "item": "potion homework"
            },
        
        "Girl's Bathroom":{
            "basement": "The Potion Classroom",
            "floor 1": "Hall",
            "floor 3": "Gryffindor Commonroom",
            "floor 4": "Transfiguration Classroom",
            "floor 5": "Headmaster's Office",
            },
        
        "Gryffindor Commonroom": {
            "basement": "The Potion Classroom",
            "floor 1": "Hall",
            "floor 2": "Girl's Bathroom",
            "floor 4": "Transfiguration Classroom",
            "floor 5": "Headmaster's Office",
            "item": "transfiguration essay",

            },
      "Transfiguration Classroom": {
            "basement": "The Potion Classroom",
            "floor 1": "Hall",
            "floor 2": "Girl's Bathroom",
            "floor 3": "Gryffindor Commonroom",
            "floor 5": "Headmaster's Office",
            },
        "Headmaster's Office": {
            "basement": "The Potion Classroom",
            "floor 1": "Hall",
            "floor 2": "Girl's Bathroom",
            "floor 3": "Gryffindor Commonroom",
            "floor 4": "Transfiguration Classroom",
            "item": "sword of gryffindor"
            }
        }

currentfloor = "Hall"

instructions()

while True:
    showStatus()

    move = ''
    while move == '':
        move = input('>')

    move = move.lower().split(" ", 1)
    if move[0] == "go":
        if move[1] in floors[currentfloor]:
            currentfloor = floors[currentfloor][move[1]]
        else:
            print('You can\'t go there!')
    if move[0] == 'get':
        if "item" in floors[currentfloor] and move[1] in floors[currentfloor]['item']:
            bag += [move[1]]
            print(move[1].capitalize() + ' got!')
            del floors[currentfloor]['item']
        else:
            print('Can\'t get ' + move[1] + '!')
    if currentfloor == "Girl's Bathroom" and 'sword of gryffindor' in bag:
        print('You have discovered the Chamber of Secrets...and, you defeated the basilisk using the Sword of Gryffindor... YOU WIN!')
        break
    elif currentfloor == 'Girl\'s Bathroom' and 'sword of gryffindor' not in bag:
        print('THE CHAMBER OF SECRETS HAS BEEN OPENED. ENEMIES OF THE HEIR, BEWARE... GAME OVER.')
        break
    elif currentfloor == 'The Potion Classroom' and 'potion homework' not in bag:
        print('You are in the Potion classroom. You said hi to Professor Snape. Gryffindor - 100 points!')
        break
    elif currentfloor == 'The Potion Classroom' and 'potion homework' in bag:
        print('You are in the Potion class. Professor Snape thinks your homework is too perfect so you must have cheated... Gryffindor - 50 points...')
        break
    elif currentfloor == 'Transfiguration Classroom' and 'transfiguration essay' not in bag:
        print('You are in Transfiguration, but where is your essay? You have failed Professor McGonagall\'s class... GAME OVER.')
        break



